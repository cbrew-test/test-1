/**
 * Returns an array of numbers after sorting the input in ascending order
 *
 * @param {number[]} arr list of integers to sort
 * @returns {number[]} list of number in `arr` after sorting
 */
function mergeSort(arr) {
    if (arr.length < 2) return arr;

    const middle = Math.floor(arr.length / 2);
    const leftHalf = arr.slice(0, middle);
    const rightHalf = arr.slice(middle);

    return merge(mergeSort(leftHalf), mergeSort(rightHalf));
}

/**
 * This is the core part of a merge sort, the two slice given to it are merged
 * in such a way that the resulting array is always sorted in ascending order
 *
 * @param {number[]} leftHalf left half of the array slice
 * @param {number[]} rightHalf rightHalf of the array slice
 *
 * @returns {number[]} an array containing all the elements from `leftHalf` and
 * `rightHalf` merged in an ascending sorted manner
 */
function merge(leftHalf, rightHalf) {
    /** @type {number[]} */
    let result = [];
    let leftIndex = 0;
    let rightIndex = 0;

    while (leftIndex < leftHalf.length && rightIndex < rightHalf.length) {
        if (leftHalf[leftIndex] < rightHalf[rightIndex]) {
            result.push(leftHalf[leftIndex]);
            leftIndex++;
        } else {
            result.push(rightHalf[rightIndex]);
            rightIndex++;
        }
    }

    return result
        .concat(leftHalf.slice(leftIndex))
        .concat(rightHalf.slice(rightIndex));
}

const inputNumber = 8046957321;
const inputArray = `${inputNumber}`
    .split("")
    .map((char) => +char)
    .filter((num) => num == 0 || num);

const sortedArray = mergeSort(inputArray);
console.log(sortedArray);
