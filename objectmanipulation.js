/**
 * @typedef Student
 * @prop {string} name Name of the student
 * @prop {number} score The marks scored by the student
 */

/**
 * Takes a list of students and a minimum value and returns a list of names of
 * the students who scored greater than the given threshold.
 *
 * @param {Student[]} students the list of students
 * @param {number} minThreshold the minimum threshold value to compare against
 * @returns {string[]} a list of names of all students who scored above `minThreshold`
 */
function getHighScores(students, minThreshold) {
  return students
    .filter((student) => student.score > minThreshold)
    .map((student) => student.name);
}

/**
 * The test array for students
 *
 * @type {Student[]}
 */
const students = [
  { name: "Alice", score: 85 },
  { name: "Bob", score: 60 },
  { name: "Charlie", score: 92 },
  { name: "David", score: 75 },
];

console.log(getHighScores(students, 70));
