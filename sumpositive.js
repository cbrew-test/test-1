/**
 * Takes an array as an input and returs the sum of all positive numbers in the array
 *
 * @param {number[]} arr the array containing the numbers to deal with
 * @returns {number} sum of all positive numbers in the array
 */
function sumOfPositives(arr) {
  const arrPositive = arr.filter((item) => item > 0);
  const sumPositive = arrPositive.reduce(
    (item, accumulator) => (accumulator += item),
    0,
  );

  return sumPositive;
}

/** array of numbers to be tested
 *
 * @type {number[]}
 */
const arr = [-2, 5, 3, -8, 10, -4];

console.log(sumOfPositives(arr));
