/// ## Var bindings
///
/// Var bindings are function scoped bindings that can be accessed any where within
/// the scope of a function, if defined locally or globally if defined that way

var someVariable = 1;
function testFunction() {
  console.log(someVariable); // prints the value of someVariable (1)
}

function testFunctionLocalScoping() {
  // once a variable is defined with var binding in a function it can be
  // accessed anywhere within that function, its value defaults to `undefined`
  // when accessed before any value is assigned to it.
  console.log(anotherVariable); // prints undefined

  var anotherVariable = 2;
  console.log(anotherVariable); // prints 2
}

/// -------------------------------------------------------------------------------
/// ## Let bindings
///
/// Let bindings are block scoped, or lexically scoped, i.e, accessing their values
/// before their declarations is a lexical violation, and it will cause an error.
/// Let bindings are only available in the scope they are declared in

function testLetBindings() {
  console.log(a); // errors due to `a` being defined by a let binding
  let a = 1;

  console.log(a); // prints 1

  {
    let b = 2;
    console.log(b); // prints 2
  }

  console.log(b); // errors since `b` is accessed outside of its scope
}

/// TODO: commit changes
/// -------------------------------------------------------------------------------
/// ## Const bindings
///
/// Const bindings are block scoped just like let bindings, the only difference is,
/// once a variable is declared with `const` it can never be re-assigned. That isn't
/// to mean that the value initialized can never be modified. `const` can only prevent
/// against assignment but not against modification, for that you must use
/// `Object.freeze`.

const a = 1;
const b = { a: 1, b: 2 };
function testConstBindings() {
  console.log(a); // prints 1
  a = 2; // errors since a is declared with a let binding

  console.log(b); // prints { a: 1, b: 2 }
  b.a = 3; // does not error since const only prevents against re-assignment

  console.log(b); // prints { a: 3, b: 2 }

  {
    const c = 2;
    console.log(c); // prints 2
  }

  console.log(c); // errors since `c` is accessed out of scope
}
