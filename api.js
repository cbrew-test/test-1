/**
 * The root of the target api from which to fetch the posts
 */
const API_TARGET = "https://jsonplaceholder.typicode.com";

/**
 * @typedef PostRequired
 * @prop {string} title The title of the post
 *
 * Retrives the posts from `API_TARGET/posts` and returns a json object representing those posts
 *
 * @returns {PostRequired[]} a json object containing the all post data
 */
async function getApiPosts() {
    const response = await fetch(`${API_TARGET}/posts`);
    if (response.status >= 300) {
        const errMessage = `the api could not find any posts, failed with status: ${response.status}`;
        throw new Error(errMessage);
    }

    return await response.json();
}

getApiPosts()
    .then((data) => {
        if (!Array.isArray(data))
            throw new Error("invalid response from api, please retry");
        else return data;
    })
    .then((data) => {
        const posts = data.slice(0, 5);
        const titles = posts.map((post) => post.title);
        console.log(titles);
    })
    .catch((err) => console.error(err));
