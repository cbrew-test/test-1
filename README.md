# CBREW.TEST.1

This repository contains all the code and markdown files that were written for the first test in code brew labs.

## Q1: Array manipulation

File: `sumpositive.js`<br />

How it was run:

```bash
node sumpositive.js
```

## Q2: Object Manipulation

File: `objectmanipulation.js`<br />

How it was run:

```bash
node objectmanipulation.js
```

## Q3: Sort number in ascending order by its digits

File: `sort.js`<br />

How it was run:

```bash
node sort.js
```

## Q4: Asynchronous programming and error handling in javascript

File: `api.js`<br />
How it was run:

```bash
python -m http.server & disown

xdg-open ./index.html # Linux
open ./index.html # Mac-os
```

## Q5: ES6 Features

Files: `es6features.js`, `es6features.md`
